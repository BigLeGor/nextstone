FROM node:8-stretch

WORKDIR /usr/local/nextstone
COPY . ./
RUN npm i -g npm@latest\
    && npm i\
    && npm cache clean -f

EXPOSE 3000
ENTRYPOINT ["npm", "run", "start:production"]