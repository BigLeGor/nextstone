import { cardTitle } from '../../../material-kit-react.jsx';

const postStyle = {
	cardTitle,
	textCenter: {
		textAlign: 'center'
	},
	textMuted: {
		color: '#6c757d'
	},
};

export default postStyle;
