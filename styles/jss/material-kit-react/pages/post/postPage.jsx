import blogPageStyle from '../blog/blogPage';

const postPageStyle = {
	...blogPageStyle
};

export default postPageStyle;
