import { container } from '../../../material-kit-react.jsx';

const loginPageStyle = {
	container: {
		...container,
		zIndex: '2',
		position: 'relative',
		paddingTop: '20vh',
		color: '#FFFFFF'
	},
	cardHidden: {
		opacity: '0',
		transform: 'translate3d(0, -60px, 0)'
	},
	pageHeader: {
		'minHeight': '100vh',
		'height': 'auto',
		'display': 'inherit',
		'position': 'relative',
		'margin': '0',
		'padding': '0',
		'border': '0',
		'alignItems': 'center',
		'&:before': {
			background: 'rgba(0, 0, 0, 0.5)'
		},
		'&:before,&:after': {
			position: 'absolute',
			zIndex: '1',
			width: '100%',
			height: '100%',
			display: 'block',
			left: '0',
			top: '0',
			content: '""'
		}
	}
};

export default loginPageStyle;
