import sharedStyle from '../sharedStyle';

const homePageStyle = {
    ...sharedStyle
};

export default homePageStyle;