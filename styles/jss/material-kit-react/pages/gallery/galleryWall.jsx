import { container } from '../../../material-kit-react.jsx';
import cardStyle from '../../components/cardStyle';

const galleryWallStyle = {
	...container,
	card: {
    	minWidth: 275,
		marginBottom: '30px',
		...cardStyle
	}
};

export default galleryWallStyle;
