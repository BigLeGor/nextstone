import imagesStyles from '../../imagesStyles.jsx';
import { cardTitle } from '../../../material-kit-react.jsx';

const galleryStyle = {
	...imagesStyles,
	cardTitle,
};

export default galleryStyle;
