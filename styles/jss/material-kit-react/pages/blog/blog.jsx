import { container } from '../../../material-kit-react.jsx';
import cardStyle from '../../components/cardStyle';

const blogStyle = {
	section: {
		background: '#EEEEEE',
		padding: '70px 0'
	},
	container,
	textCenter: {
		textAlign: 'center'
	},
	card: {
    	minWidth: 275,
		marginBottom: '30px',
		...cardStyle,
		"&:hover": {
			borderColor: '#0bf',
			borderRadius: '10px',
			cursor: 'pointer',
			border: '3px solid #fff',
			marginBottom: '24px',
		}
	},
	bullet: {
		display: 'inline-block',
		margin: '0 2px',
		transform: 'scale(0.8)',
	},
	title: {
		marginBottom: 16,
		fontSize: 14,
	},
	pos: {
		marginBottom: 12,
	}
};

export default blogStyle;
