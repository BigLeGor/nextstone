import { container, title } from '../../../material-kit-react.jsx';

const blogPageStyle = {
	container,
	main: {
		background: '#FFFFFF',
		position: 'relative',
		zIndex: '3',
		minHeight: '100px'
	},
	mainRaised: {
		margin: '-60px 30px 0px',
		borderRadius: '6px',
		boxShadow:
      '0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)'
	},
	title: {
		...title,
		marginTop: '30px',
		minHeight: '32px',
		textDecoration: 'none'
	},
	loading: {
		top: '50%',
		left: '50%',
		margin: '-20px 0 0 -20px',
		position: 'absolute'
	},
	link: {
		textDecoration: 'none'
	},
	textCenter: {
		textAlign: 'center'
	}
};

export default blogPageStyle;
