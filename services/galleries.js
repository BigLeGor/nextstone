const keystone = require('keystone');
const { galleryResSchema } = require('./utils');

const Gallery = keystone.list('Gallery');

const galleriesService = async (categoryIds) => {
	try {
		var results;
		if (categoryIds && Array.isArray(categoryIds) && categoryIds.length > 0) {
			results = await Gallery.model
				.find()
				.populate('author')
				.where('state', 'published')
				.where('categories').in(categoryIds)
				.sort('-publishedDate')
				.exec() || [];
		} else {
			results = await Gallery.model
				.find()
				.populate('author')
				.where('state', 'published')
				.sort('-publishedDate')
				.exec() || [];
		}

		return results.map((post) => {
			const {
				_id,
				__v,
				state,
				author: { email, name },
				...galleryResObj
			} = post.toJSON();

			return Object.assign({}, galleryResSchema, galleryResObj, { author: { email, name } });
		});
	} catch (err) {
		// log here
		console.log(err);
		if (err) throw err;
		return [];
	}
};

module.exports = galleriesService;
