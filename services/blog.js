const keystone = require('keystone');
const { postResSchema } = require('./utils');

const Post = keystone.list('Post');

const blogService = async (categoryIds) => {
	try {
		var results;
		if (categoryIds && Array.isArray(categoryIds) && categoryIds.length > 0) {
			results = await Post.model
				.find()
				.populate('author')
				.where('state', 'published')
				.where('categories').in(categoryIds)
				.sort('-publishedDate')
				.exec() || [];
		} else {
			results = await Post.model
				.find()
				.populate('author')
				.where('state', 'published')
				.sort('-publishedDate')
				.exec() || [];
		}

		return results.map((post) => {
			const {
				_id,
				__v,
				state,
				author: { email, name },
				content: { brief },
				...postResObj
			} = post.toJSON();

			return Object.assign({}, postResSchema, postResObj, { author: { email, name }, content: { brief } });
		});
	} catch (err) {
		// log here
		console.log(err);
		if (err) throw err;
		return [];
	}
};

module.exports = blogService;
