const buildCategoryService = require('./category');

const keystone = require('keystone');

const PostCategory = keystone.list('PostCategory');

const postCategoryService = buildCategoryService(PostCategory);

module.exports = postCategoryService;
