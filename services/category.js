const buildCategoryService = (categoryKSList) => async (categoryIds) => {
	try {
		const results = await categoryKSList.model
			.find()
			.where('internal')
			.ne(true)
			.exec() || [];

		return results.map((category) => {
			const {
				_id,
				__v,
				...categoryResObj
			} = category.toJSON();

			return Object.assign({}, categoryResObj);
		});
	} catch (err) {
		// log here
		if (err) throw err;
		return [];
	}
};

module.exports = buildCategoryService;

