const buildCategoryService = require('./category');

const keystone = require('keystone');

const GalleryCategory = keystone.list('GalleryCategory');

const galleryCategoryService = buildCategoryService(GalleryCategory);

module.exports = galleryCategoryService;

