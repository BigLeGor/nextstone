const keystone = require('keystone');

const {
	session: {
		signin,
		signout,
		keystoneAuth
	}
} = keystone;

const User = keystone.list('User');

const userValidateService = async (username, email, password) => {
	try {
		const user = await User.model
			.findOne({
				name: username,
				email,
				password
			})
			.exec();

		if (!user) {
			return null;
		}

		return user.toJSON();
	} catch (err) {
		// log here
		if (err) throw err;
	}
};

const signinService = (req, res) => ({ email, password }) =>
	new Promise((resolve, reject) =>
		signin(
			{ email: email, password: password },
			req, res,
			(user) => resolve(user),
			(err) => reject(err)
		)
	);

const signoutService = (req, res) => new Promise((resove) => signout(req, res, () => resove(true)));

const authService = keystoneAuth;

module.exports = {
	authService,
	userValidateService,
	signinService,
	signoutService
};
