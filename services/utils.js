const postResSchema = {
	title: '',
	author: {},
	image: '',
	publishedDate: '',
	categories: [],
	content: {}
};

const galleryResSchema = {
	name: '',
	author: {},
	thumb: '',
	video: '',
	publishedDate: '',
	categories: [],
	brief: ''
};

module.exports = {
	postResSchema,
	galleryResSchema
};
