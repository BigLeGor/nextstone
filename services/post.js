const keystone = require('keystone');
const { postResSchema } = require('./utils');

const Post = keystone.list('Post');

const postService = async (postSlug) => {
	try {
		const result = await Post.model
			.findOne({
				slug: postSlug,
				state: 'published'
			})
			.populate('author')
			.exec();

		const post = result.toJSON();
		const {
			_id,
			__v,
			slug,
			state,
			content: { extended },
			author: { email, name },
			...postRes
		} = post;

		return Object.assign({}, postResSchema, postRes, { author: { email, name }, content: { extended } });
	} catch (err) {
		// log here
		if (err) throw err;
		// return postResSchema;
	}
};

module.exports = postService;
