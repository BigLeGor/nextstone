const getNextRoutes = require('next-routes');

const opt = {
	// add custom link or router component here
	// Link: require('./my/link')
	// Router: require('./my/router')
};

const routes = getNextRoutes(opt);

routes
	.add('post', '/post/:slug')
	.add('user', '/user/:id', 'profile')
	.add('blog', '/blog/:categoryId?')
	.add('galleryWall', '/galleryWall/:categoryId?');

module.exports = routes;
