const fs = require('fs');
const path = require('path');

const apiPath = path.join(__dirname, 'api');

const loadAPI = () => {
	const apiFiles = fs.readdirSync(apiPath);
	return apiFiles.map((apiFile) => require(`./api/${apiFile}`));
};

const apiUrlPrefix = 'api';

module.exports = {
	loadAPI,
	apiUrlPrefix
};
