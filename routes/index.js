const { loadAPI, apiUrlPrefix } = require('./loadAPI');
const pageRouters = require('./pages');
// const { authService } = require('../services/permission');

const apiMethodsHandler = Object.freeze({
	get: (target, property) => {
		if (property in target) {
			return target[property];
		}
		// if wanna custome the error message/ response, do as below:
		// return (_req, res, next) => res.status(404).json({ error: 'not found' });

		// show the 404 page
		return (_req, res, _next) => res.status(404).json({ code: 404, error: 'not found' });
	}
});

const apiRegister = (keystoneApp) => ({ apiPath, handler }) => {
	const proxyHandler = new Proxy(handler, apiMethodsHandler);
	return {
		registGet: () => keystoneApp.get(`/${apiUrlPrefix}${apiPath}`, proxyHandler.get),
		registPost: () => keystoneApp.post(`/${apiUrlPrefix}${apiPath}`, proxyHandler.post),
		registPut: () => keystoneApp.put(`/${apiUrlPrefix}${apiPath}`, proxyHandler.put),
		registDelete: () => keystoneApp.delete(`/${apiUrlPrefix}${apiPath}`, proxyHandler.delete)
	};
};

const checkAuth = (req, res, next) => {
	// you could check user permissions here too
	if (req.user) return next();
	return res.status(403).json({ error: 'no access' });
};

// or just simple use authservice here, just the same as keystoneAuth
// const checkAuth = authService;

// Setup Route Bindings
exports = module.exports = (nextApp) => (keystoneApp) => {

	// Next request handler
	const handler = pageRouters.getRequestHandler(nextApp);

	const allApi = loadAPI();
	const register = apiRegister(keystoneApp);
	// we could add a filter to select the api we need and regist the needed methods, e.g. both get and post...
	allApi.forEach((api) => register(api).registGet());


	// add an API endpoint for signing in _before_ your protected routes
	// keystoneApp.post('/api/signin', signin);
	// keystoneApp.post('/api/signout', signout);
	allApi.filter(({ apiPath }) =>
		['/signin', '/signout'].includes(apiPath)
	).forEach((api) => register(api).registPost());

	// then bind that middleware in your routes before any paths
	// that should be protected
	// keystoneApp.all('/api*', checkAuth);

	// the rest endpoints
	keystoneApp.get('*', (req, res) => handler(req, res));
};
