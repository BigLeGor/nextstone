const postService = require('../../services/post');

const apiPath = '/home';

const handler = {
	get: async (_req, res, _next) => {
		const homepagecontent = 'homepagecontent';
		try {
			const { content: { extended: response } } = await postService(homepagecontent);
			res.json(response);
		} catch (err) {
			// if (err) throw err;
			return res.status(404).json({ code: 404, error: 'no found' });
		}
	}
};

module.exports = {
	apiPath,
	handler
};

