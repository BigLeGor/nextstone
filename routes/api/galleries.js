const galleriesService = require('../../services/galleries');

const apiPath = '/galleries/:categories?';

const handler = {
	get: async (req, res, _next) => {
		const { categories } = req.params;

		try {
			let galleries;

			if (categories) {
				galleries = await galleriesService([categories]);
			} else {
				galleries = await galleriesService();
			}
			res.json(galleries);
		} catch (err) {
			// if (err) throw err;
			return res.status(404).json({ code: 404, error: 'no found' });
		}
	}
};

module.exports = {
	apiPath,
	handler,
};

