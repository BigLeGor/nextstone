const blogService = require('../../services/blog');

const apiPath = '/blog/:categories?';

const handler = {
	get: async (req, res, next) => {
		const { categories } = req.params;

		try {
			let blogPosts;

			if (categories) {
				blogPosts = await blogService([categories]);
			} else {
				blogPosts = await blogService();
			}
			res.json(blogPosts);
		} catch (err) {
			// if (err) throw err;
			return res.status(404).json({ code: 404, error: 'no found' });
		}
	}
};

module.exports = {
	apiPath,
	handler,
};

