const postCategoryService = require('../../services/postCategory');
const galleryCategoryService = require('../../services/galleryCategory');

const apiPath = '/menus';

const handler = {
	get: async (_req, res, _next) => {
		try {
			const [
				postCategories,
				galleryCategories
			] = await Promise.all([
				postCategoryService(),
				galleryCategoryService()
			]);
			res.json({
				postCategories,
				galleryCategories
			});
		} catch (err) {
			if (err) throw err;
		}
	}
};

module.exports = {
	apiPath,
	handler,
};

