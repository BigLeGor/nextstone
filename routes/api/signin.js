const { userValidateService, signinService } = require('../../services/permission');

const apiPath = '/signin';

const handler = {
	post: async (req, res, next) => {
		const { body: { username, email, password } } = req;
		if (!username || !email || !password) {
			return res.json({ success: false });
		}

		try {
			const validUser = await userValidateService(username, email, password);
			if (!validUser) {
				return res.json({
					success: false,
					session: false,
					message: 'No such a user'
				});
			}

			try {
				const { id: userId } = await signinService(req, res)(validUser);
				res.json({
					success: true,
					session: true,
					date: new Date().getTime(),
					userId
				});
			} catch (err) {
				res.json({
					success: true,
					session: false,
					message: (err && err.message ? err.message : false) || 'Sorry, there was an issue signing you in, please try again.'
				});
			}
		} catch (err) {
			res.json({
				success: false,
				session: false,
				message: (err && err.message ? err.message : false) || 'Sorry, there was an issue signing you in, please try again.'
			});
		}
	}
};

module.exports = {
	apiPath,
	handler
};

