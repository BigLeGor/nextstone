const postService = require('../../services/post');

const apiPath = '/post/:postSlug';

const handler = {
	get: async (req, res, next) => {
		const { postSlug } = req.params;
		try {
			const response = await postService(postSlug);
			res.json(response);
		} catch (err) {
			// if (err) throw err;
			return res.status(404).json({ code: 404, error: 'no found' });
		}
	}
};

module.exports = {
	apiPath,
	handler
};

