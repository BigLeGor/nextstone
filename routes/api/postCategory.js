const postCategoryService = require('../../services/postCategory');

const apiPath = '/postCategory';

const handler = {
	get: async (_req, res, _next) => {
		try {
			const postCategories = await postCategoryService();
			res.json(postCategories);
		} catch (err) {
			if (err) throw err;
		}
	}
};

module.exports = {
	apiPath,
	handler,
};

