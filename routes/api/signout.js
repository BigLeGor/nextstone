const { signoutService } = require('../../services/permission');

const apiPath = '/signout';

const handler = {
	post: async (req, res, next) => {
		try {
			const signedout = await signoutService(req, res);
			res.json({ signedout });
		} catch (err) {
			if (err) throw err;
		}
	}
};

module.exports = {
	apiPath,
	handler
};
