const MenusNameSpace = 'Menus';

const DEFAULT_MENUS_STATE = [];

const MenusApi = `${!process.browser ? process.env.API_HOST : ''}/api/menus`;

export {
	MenusNameSpace,
	DEFAULT_MENUS_STATE,
	MenusApi
};
