const PostCategoryNameSpace = 'PostCategory';

const DEFAULT_POST_CATEGORY_STATE = {
	isFetching: false,
	PostCategories: []
};

const PostCategoryApi = `${!process.browser ? process.env.API_HOST : ''}/api/postCategory`;

export {
	PostCategoryNameSpace,
	DEFAULT_POST_CATEGORY_STATE,
	PostCategoryApi
};
