const PostNameSpace = 'Post';

const DEFAULT_DETAIL_STATE = {
	title: '',
	author: {},
	image: '',
	publishedDate: '',
	categories: [],
	content: {}
};

const DEFAULT_POST_STATE = {
	isFetching: false,
	detail: DEFAULT_DETAIL_STATE
};

const PostApi = `${!process.browser ? process.env.API_HOST : ''}/api/post`;

export {
	PostNameSpace,
	DEFAULT_POST_STATE,
	PostApi
};
