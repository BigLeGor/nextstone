const HomeApi = `${!process.browser ? process.env.API_HOST : ''}/api/home`;

export {
	HomeApi
};
