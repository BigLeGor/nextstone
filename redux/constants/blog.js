const BlogNameSpace = 'blog';

const DEFAULT_POSTS_STATE = {
	/* "slug-1": {
        content: {
            brief: 'mock post with slug 1'
        },
    }*/
};

const DEFAULT_BLOG_STATE = {
	isFetching: false,
	posts: DEFAULT_POSTS_STATE
};

const BlogApi = '/api/blog';

export {
	BlogApi,
	BlogNameSpace,
	DEFAULT_BLOG_STATE,
	DEFAULT_POSTS_STATE
};
