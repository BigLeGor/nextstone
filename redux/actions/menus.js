import axios from 'axios';
import { namespaceConfig } from 'fast-redux';
import { DEFAULT_MENUS_STATE, MenusNameSpace, MenusApi } from '../constants/menus';

const {
	action: menusAction,
	getState: getMenusState
} = namespaceConfig(MenusNameSpace, DEFAULT_MENUS_STATE);

const requestMenus = () => axios.get(MenusApi);

const receiveMenus = menusAction(
	'receiveMenus',
	(state, menus) => {
		const postCategories = menus.postCategories || [];
		const galleryCategories = menus.galleryCategories || [];

		return [
			...state,
			...postCategories,
			{
				galleryMenu: [
					...galleryCategories
				]
			}
		];
	}
);

export {
	getMenusState,
	requestMenus,
	receiveMenus
};
