import axios from 'axios';
import { HomeApi } from '../constants/home';

const fetchHomePage = () => axios.get(HomeApi);

export {
	fetchHomePage
};
