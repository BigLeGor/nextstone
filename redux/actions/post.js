import axios from 'axios';
import { namespaceConfig, staticPropertyConfig } from 'fast-redux';
import { DEFAULT_POST_STATE, PostNameSpace, PostApi } from '../constants/post';
// import { sleep } from '../../lib/utils';

const { action: postAction } = namespaceConfig(PostNameSpace, DEFAULT_POST_STATE);

const [fetchingKey, postDetialKey] = Object.keys(DEFAULT_POST_STATE);

const {
	propertyAction: fetchAction,
	getPropertyState: getFetchState
} = staticPropertyConfig(postAction, fetchingKey);

const {
	propertyAction: postDetailAction,
	getPropertyState: getPostState
} = staticPropertyConfig(postAction, postDetialKey);

const isFetchingPost = fetchAction('requestPost', (_state, isFetching) => isFetching);

const receivePost = postDetailAction('receivePost', (state, postDatil) => ({ ...state, ...postDatil }));

const axiosInst = axios.create({
	baseURL: PostApi
});

const requestPost = (postSlug) => axiosInst.get(postSlug);

const fetchPost = (postSlug) => async (dispatch) => {
	dispatch(isFetchingPost(true));
	// await sleep(5);
	const { data: post } = await requestPost(postSlug);
	dispatch(isFetchingPost(false));
	dispatch(receivePost(post));
};

export {
	getFetchState,
	getPostState,
	fetchPost,
	requestPost,
	receivePost
};
