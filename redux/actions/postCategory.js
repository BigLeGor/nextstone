import axios from 'axios';
import { namespaceConfig, staticPropertyConfig } from 'fast-redux';
import { DEFAULT_POST_CATEGORY_STATE, PostCategoryNameSpace, PostCategoryApi } from '../constants/postCategory';
import { sleep } from '../../lib/utils';

const { action: postCategoryAction } = namespaceConfig(PostCategoryNameSpace, DEFAULT_POST_CATEGORY_STATE);

const [isFetchingKey, PostCategoriesKey] = Object.keys(DEFAULT_POST_CATEGORY_STATE);

const { propertyAction: fetchAction }
	= staticPropertyConfig(postCategoryAction, isFetchingKey);

const { propertyAction: postCategorisAction, getPropertyState: getPostCategoryState }
	= staticPropertyConfig(postCategoryAction, PostCategoriesKey);

const isFetchingPostCategories = fetchAction('requestPostCategory', (_state, isFetching) => isFetching);

// const { propertyAction: postCategoryAction } = dynamicPropertyConfig(postCategoryAction, DEFAULT_POST_CATEGORY_STATE);
// const receivePostCategories = postCategoryAction('receivePostCategories', (state, postCategoriesData) => {
// 	const postCategoriesData = { ...state };
// 	postCategoriesData.forEach(({ slug, ...post }) => Object.assign(postsState, {
// 		[slug]: { ...post }
// 	}));
// 	return postsState;
// });

const requestPostCategories = () => axios.get(PostCategoryApi);

const receivePostCategories = postCategorisAction(
	'receivePostCategory',
	(state, postCategories) => [...state, ...Array.from(postCategories || [])]
);

const fetchPostCategories = () => async (dispatch) => {
	dispatch(isFetchingPostCategories(true));
	// await sleep(2);
	const { data: postCategoiresData } = await requestPostCategories();
	dispatch(isFetchingPostCategories(false));
	// const pp = {'name':'bbbbbb'};
	// debugger
	dispatch(receivePostCategories(postCategoiresData));
};

export {
	getPostCategoryState,
	fetchPostCategories,
	requestPostCategories,
	receivePostCategories
};
