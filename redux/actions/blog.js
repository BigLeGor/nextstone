import axios from 'axios';
import { namespaceConfig, dynamicPropertyConfig, staticPropertyConfig } from 'fast-redux';
import { DEFAULT_BLOG_STATE, DEFAULT_POSTS_STATE, BlogNameSpace, BlogApi } from '../constants/blog';
import { sleep } from '../../lib/utils';

const { action: blogAction, getState: getBlogState } = namespaceConfig(BlogNameSpace, DEFAULT_BLOG_STATE);

const [isFetchingKey, postsKey] = Object.keys(DEFAULT_BLOG_STATE);

const { propertyAction: fetchAction } = staticPropertyConfig(blogAction, isFetchingKey);
const isFetchingPosts = fetchAction('requestPosts', (_state, isFetching) => isFetching);

const { propertyAction: postsAction } = dynamicPropertyConfig(blogAction, DEFAULT_POSTS_STATE);
const receivePosts = postsAction('receivePosts', (state, postsData) => {
	const postsState = { ...state };
	postsData.forEach(({ slug, ...post }) => Object.assign(postsState, {
		[slug]: { ...post }
	}));
	return postsState;
});

const axiosInst = axios.create({
	baseURL: BlogApi
});

const requestPosts = (categoryId) => axiosInst.get(categoryId);

const fetchPosts = (categoryId) => async (dispatch) => {
	dispatch(isFetchingPosts(true));
	// await sleep(5);

	// const { data: postsData } = await axios.get(BlogApi);

	const { data: postsData } = await requestPosts(categoryId);
	dispatch(isFetchingPosts(false));
	dispatch(receivePosts(postsKey, postsData));
};

export {
	getBlogState,
	fetchPosts,
	receivePosts
};
