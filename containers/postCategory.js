import React from 'react';
// import dateFormat from 'dateformat';
// import Card from '../components/Card/Card.js';
// import CardBody from '../components/Card/CardBody.js';
// import CardHeader from '../components/Card/CardHeader.js';
// import CardFooter from '../components/Card/CardFooter.js';
import { withStyles } from '@material-ui/core/styles';

//import postStyle from '../styles/jss/material-kit-react/pages/post/post';

import blogStyle from '../styles/jss/material-kit-react/pages/blog/blog';

const PostCategory = ({ classes, postCategories = {} }) => (
    <div>
        <p>This is the Post Category page!!</p>
        <p>
                            {
                            
						Object.values(postCategories).map(
							({key, name}) => {
								return key + name
                            }
                        )
					}
        </p>
        
    </div>
);

export default withStyles(blogStyle)(PostCategory);
