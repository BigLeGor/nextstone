import Gallery from './gallery';
import GridContainer from '../components/Grid/GridContainer';
import GridItem from '../components/Grid/GridItem';
import { withStyles } from '@material-ui/core/styles';

import blogStyle from '../styles/jss/material-kit-react/pages/blog/blog';

const GalleryWall = ({ classes }) => (
	<div className={classes.section}>
		<div className={classes.container}>
			<div id="nav-tabs">
				<GridContainer>
					<GridItem xs={12} sm={12} md={6}>
						<Gallery />
					</GridItem>
					<GridItem xs={12} sm={12} md={6}>
						<Gallery />
					</GridItem>
					<GridItem xs={12} sm={12} md={6}>
						<Gallery />
					</GridItem>
					<GridItem xs={12} sm={12} md={6}>
						<Gallery />
					</GridItem>
				</GridContainer>
			</div>
		</div>
	</div>
);

export default withStyles(blogStyle)(GalleryWall);
