import React from 'react';
// material-ui components
import withStyles from '@material-ui/core/styles/withStyles';
// core components
import Card from '../components/Card/Card';
import galleryStyle from '../styles/jss/material-kit-react/pages/gallery/gallery';

const imgSrc = '/static/img/video.svg';

const Gallery = ({ classes }) => (
	<Card>
		<img className={classes.imgCard} src={imgSrc} alt="Card-img" />
		<div className={classes.imgCardOverlay}>
			<h4 className={classes.cardTitle}>Card title</h4>
			<p>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
			<p>Last updated 3 mins ago</p>
		</div>
	</Card>
);

export default withStyles(galleryStyle)(Gallery);
