import React from 'react';
import GridContainer from '../components/Grid/GridContainer.js';
import GridItem from '../components/Grid/GridItem.js';
import Button from '../components/CustomButtons/Button.js';
import Post from './post';
import { Link } from '../routes/pages';
import ReactHtmlParser from 'react-html-parser';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { withStyles } from '@material-ui/core/styles';
import dateFormat from 'dateformat';

import blogStyle from '../styles/jss/material-kit-react/pages/blog/blog';

const Blog = ({ classes, posts }) => (
	<div className={classes.section}>
		<div className={classes.container}>
			<div id="nav-tabs">
				<GridContainer>
					{
						Object.entries(posts).map(
							([slug, post]) => (
								<GridItem xs={12} sm={12} md={12}>
									<Link href={`/post/${slug}`}>
										<Card className={classes.card}>
											<CardContent>
												<Typography className={classes.title} color="textSecondary">
													{post.publishedDate && dateFormat(new Date(post.publishedDate))}
												</Typography>
												<Typography variant="headline" component="h2">
													{post.title}
												</Typography>
												<Typography className={classes.pos} color="textSecondary">
													{`${post.author.name.first} ${post.author.name.last}`}
												</Typography>
												<Typography component="p">
													{ReactHtmlParser(post.content.brief)}
												</Typography>
											</CardContent>
										</Card>
									</Link>
									{
										/* <Post slug={slug} post={post}>
										{ReactHtmlParser(post.content.brief)}
										<Link href={`/post/${slug}`}>
											<Button color="facebook">Read More</Button>
										</Link>
									</Post> */
									}
								</GridItem>
							)
						)
					}
				</GridContainer>
			</div>
		</div>
	</div>
);

export default withStyles(blogStyle)(Blog);
