const bgImg = '/static/img/homeBg.jpg';

const Home = ({ children }) => (
	<div style={{
		backgroundImage: 'url(' + bgImg + ')',
		backgroundSize: 'cover',
		backgroundPosition: 'top center',
		padding: '15px',
	}}>
		{children}
	</div>
);

export default Home;
