import React from 'react';
import dateFormat from 'dateformat';
import Card from '../components/Card/Card.js';
import CardBody from '../components/Card/CardBody.js';
import CardHeader from '../components/Card/CardHeader.js';
import CardFooter from '../components/Card/CardFooter.js';
import { withStyles } from '@material-ui/core/styles';

import postStyle from '../styles/jss/material-kit-react/pages/post/post';

const authorPropHandler = Object.freeze({
	get: (target, property) => {
		target[property] = target[property] || '';

		if (typeof target[property] === 'object') {
			return new Proxy(target[property], authorPropHandler);
		}

		return target[property];
	}
});

const Post = ({ classes, children, post: { title, publishedDate, author } = {} }) => {
	const authorProxy = new Proxy(author, authorPropHandler);
	return (
		<Card className={classes.textCenter}>
			<CardHeader color="info">{title}</CardHeader>
			<CardBody>
				<h4 className={classes.cardTitle}>{`${authorProxy.name.first} ${authorProxy.name.last}`}</h4>
				{children}
			</CardBody>
			<CardFooter className={classes.textMuted}>
				{publishedDate && dateFormat(new Date(publishedDate))}
			</CardFooter>
		</Card>
	);
};

export default withStyles(postStyle)(Post);
