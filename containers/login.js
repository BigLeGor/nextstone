import React from 'react';
// @material-ui/core components
import InputAdornment from '@material-ui/core/InputAdornment';
// @material-ui/icons
import Email from '@material-ui/icons/Email';
import LockOutline from '@material-ui/icons/LockOutline';
import People from '@material-ui/icons/People';
import GridContainer from '../components/Grid/GridContainer';
import GridItem from '../components/Grid/GridItem';
import Button from '../components/CustomButtons/Button';
import Card from '../components/Card/Card';
import CardBody from '../components/Card/CardBody';
import CardHeader from '../components/Card/CardHeader';
import CardFooter from '../components/Card/CardFooter';
import CustomInput from '../components/CustomInput/CustomInput';

import { withStyles } from '@material-ui/core/styles';

import loginStyle from '../styles/jss/material-kit-react/pages/login/login';

const Login = ({ classes, className }) => (
	<div className={classes.container}>
		<GridContainer justify="center">
			<GridItem xs={12} sm={12} md={4}>
				<Card className={className}>
					<form className={classes.form}>
						<CardHeader color="primary" className={classes.cardHeader}>
							<h4>Login</h4>
							<div className={classes.socialLine}>
								<Button
									justIcon
									href="#pablo"
									target="_blank"
									color="transparent"
									onClick={e => e.preventDefault()}
								>
									<i className="fab fa-twitter" />
								</Button>
								<Button
									justIcon
									href="#pablo"
									target="_blank"
									color="transparent"
									onClick={e => e.preventDefault()}
								>
									<i className="fab fa-facebook" />
								</Button>
								<Button
									justIcon
									href="#pablo"
									target="_blank"
									color="transparent"
									onClick={e => e.preventDefault()}
								>
									<i className="fab fa-google-plus-g" />
								</Button>
							</div>
						</CardHeader>
						<p className={classes.divider}>Or Be Classical</p>
						<CardBody>
							<CustomInput
								labelText="First Name..."
								id="first"
								formControlProps={{
									fullWidth: true
								}}
								inputProps={{
									type: 'text',
									endAdornment: (
										<InputAdornment position="end">
											<People className={classes.inputIconsColor} />
										</InputAdornment>
									)
								}}
							/>
							<CustomInput
								labelText="Email..."
								id="email"
								formControlProps={{
									fullWidth: true
								}}
								inputProps={{
									type: 'email',
									endAdornment: (
										<InputAdornment position="end">
											<Email className={classes.inputIconsColor} />
										</InputAdornment>
									)
								}}
							/>
							<CustomInput
								labelText="Password"
								id="pass"
								formControlProps={{
									fullWidth: true
								}}
								inputProps={{
									type: 'password',
									endAdornment: (
										<InputAdornment position="end">
											<LockOutline
												className={classes.inputIconsColor}
											/>
										</InputAdornment>
									)
								}}
							/>
						</CardBody>
						<CardFooter className={classes.cardFooter}>
							<Button simple color="primary" size="lg">
								Get started
							</Button>
						</CardFooter>
					</form>
				</Card>
			</GridItem>
		</GridContainer>
	</div>
);

export default withStyles(loginStyle)(Login);
