/*
In production the stylesheet is compiled to .next/static/style.css and served from /_next/static/style.css
You have to include it into the page using either next/head or a custom _document.js, as is being done in this file.
*/

import Document, { Head, Main, NextScript } from 'next/document';
import JssProvider from 'react-jss/lib/JssProvider';
import flush from 'styled-jsx/server';
import getPageContext from '../lib/getPageContext';
import React from 'react';
import '../styles/style.scss';

class NSDocument extends Document {
	static async getInitialProps(ctx) {
		// Resolution order
		//
		// On the server:
		// 1. page.getInitialProps
		// 2. document.getInitialProps
		// 3. page.render
		// 4. document.render
		//
		// On the server with error:
		// 2. document.getInitialProps
		// 3. page.render
		// 4. document.render
		//
		// On the client
		// 1. page.getInitialProps
		// 3. page.render

		// Get the context of the page to collected side effects.
		const pageContext = getPageContext();
		const page = ctx.renderPage(Component => props => (
			<JssProvider
				registry={pageContext.sheetsRegistry}
				generateClassName={pageContext.generateClassName}
			>
				<Component pageContext={pageContext} {...props} />
			</JssProvider>
		));

		return {
			...page,
			pageContext,
			styles: (
				<React.Fragment>
					<style
						id="jss-server-side"
						// eslint-disable-next-line react/no-danger
						dangerouslySetInnerHTML={{ __html: pageContext.sheetsRegistry.toString() }}
					/>
					{flush() || null}
				</React.Fragment>
			),
		};
	}
	render() {
		return (
			<html>
				<Head>
					<title>NextStone</title>
					<meta charSet="utf-8" />
					{/* Use minimum-scale=1 to enable GPU rasterization */}
					<meta
						name="viewport"
						content={
							'width=device-width, initial-scale=1, shrink-to-fit=no, minimum-scale=1'
						}
					/>
					{/* PWA primary color */}
					<meta name="theme-color" content={this.props.pageContext.theme.palette.primary.main} />
					{/* Fonts and icons */}
					<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
					<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" />
				</Head>
				<body>
					<Main ref="root" />
					<NextScript />
					<script dangerouslySetInnerHTML={{
						__html: `if (
							${process.env.NODE_ENV === 'production'} &&
							typeof window.__REACT_DEVTOOLS_GLOBAL_HOOK__ === "object"
						) {
							for (let [key, value] of Object.entries(window.__REACT_DEVTOOLS_GLOBAL_HOOK__)) {
								window.__REACT_DEVTOOLS_GLOBAL_HOOK__[key] = typeof value == "function" ? ()=>{} : null;
							}
						}` }} />
				</body>
			</html>
		);
	}
}

export default NSDocument;
