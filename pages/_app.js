import App, { Container } from 'next/app';
import withRedux from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import withMaterialUI from '../lib/withMaterialUI';
import withError from '../lib/withError';
import { initStore } from '../lib/reduxUtils';
import { compose } from '../lib/utils';

import { getMenusState, requestMenus, receiveMenus } from '../redux/actions/menus';

const dev = process.env.NODE_ENV !== 'production';

@withRedux(initStore, { debug: dev })
class NSApp extends App {
	static async getInitialProps({ Component, ctx, router }) {

		// Keep in mind that this will be called twice on server, one for page and second for error page
		/* await new Promise((res) => {
            setTimeout(() => {
                ctx.store.dispatch({type: 'TOE', payload: 'was set in _app'});
                res();
            }, 200);
		});*/

		// could get a router instance here
		// const { pathName, asPath, query, route: routePath } = router;

		// fetch headers' data
		/* const { data: postCategoiresData } = await requestPostCategories();

		const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {}; */

		const getMenuData = () => {
			try {
				const menuState = getMenusState(ctx.store.getState());
				if (menuState.length > 0) {
					return Promise.resolve([]);
				}

				return requestMenus();
			} catch (error) {
				return Promise.resolve([]);
			}
		};

		const getCompInitProps = () =>
			Component.getInitialProps ? Component.getInitialProps(ctx) : Promise.resolve({});

		const [{ data: menusData }, pageProps] = await Promise.all([
			getMenuData(),
			getCompInitProps()
		]);

		if (menusData && Object.values(menusData).some((item) => item.length > 0)) {
			ctx.store.dispatch(receiveMenus(menusData));
		}
		return { pageProps };

	}

	render() {
		const { Component, pageProps, store } = this.props;
		const ComponentWithMaterialUI = compose(withError, withMaterialUI)(Component);
		return (
			<Container>
				<Provider store={store}>
					<ComponentWithMaterialUI {...pageProps} />
				</Provider>
			</Container>
		);
	}
}

export default NSApp;
