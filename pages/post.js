import Post from '../containers/post';
import { connect } from 'react-redux';
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import ReactHtmlParser from 'react-html-parser';
import withHeaderFooter from '../lib/withHeaderFooter';
import { showErrorPage } from '../lib/utils';
import PageTitle from '../components/PageTitle';
import { getFetchState, getPostState, fetchPost, receivePost, requestPost } from '../redux/actions/post';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';

import postPageStyle from '../styles/jss/material-kit-react/pages/post/postPage';

const mapStateToProps = (state) => ({
	isFetching: getFetchState(state),
	post: getPostState(state)
});

const mapDispatchToProps = (dispatch) => ({ fetchPost: (slug) => dispatch(fetchPost(slug)) });

@withHeaderFooter()
@connect(mapStateToProps, mapDispatchToProps)
@withStyles(postPageStyle)
class PostPage extends React.Component {
	componentDidMount() {
		if (!this.props.slug || this.props.slug === '') {
			return;
		}
	}

	render() {
		const { post, isFetching, slug, classes } = this.props;
		let content = null;
		if (isFetching) {
			content = (<CircularProgress className={classes.loading} />);
		} else {
			content = post && (
				<Post post={post}>
					{ReactHtmlParser(post.content.extended)}
				</Post>
			);
		}
		return [
			<PageTitle>{slug}</PageTitle>,
			<div className={classNames(classes.main, classes.mainRaised)}>
				{content}
			</div>
		];
	}
}

PostPage.getInitialProps = async ({ query, res, isServer, store: { dispatch } }) => {
	const { slug } = query;
	if (!slug && res) {
		return showErrorPage(res)(404);
	}
	if (isServer) {
		try {
			const { data: post } = await requestPost(slug);
			dispatch(receivePost(post));
		} catch (err) {
			let statusCode = 404;
			if (err.response.status) {
				statusCode = err.response.status;
			}
			return showErrorPage(res)(statusCode);
		}
	}
	return { slug };
};

PostPage.propTypes = {
	fetchPost: PropTypes.func,
	post: PropTypes.object,
	slug: PropTypes.string,
	isFetching: PropTypes.bool
};

export default PostPage;
