import { withStyles } from '@material-ui/core/styles';
import ReactHtmlParser from 'react-html-parser';
import Home from '../containers/home';
import withHeaderFooter from '../lib/withHeaderFooter';
import { showErrorPage } from '../lib/utils';
import { compose } from '../lib/utils';
import classNames from 'classnames';
import { fetchHomePage } from '../redux/actions/home';

import homePageStyle from '../styles/jss/material-kit-react/pages/homePage';

const HomePage = compose(withHeaderFooter('人工智能行业资讯'), withStyles(homePageStyle))(
	({ classes, homepageContent }) => (
		<div className={classNames(classes.main, classes.mainRaised)}>
			<Home>
				{ReactHtmlParser(homepageContent)}
			</Home>
		</div>
	)
);

HomePage.getInitialProps = async ({ res, isServer }) => {
	if (isServer) {
		try {
			const { data: homepageContent } = await fetchHomePage();
			return { homepageContent };
		} catch (err) {
			let statusCode = 404;
			if (err && err.response && err.response.status) {
				statusCode = err.response.status;
			}
			return showErrorPage(res)(statusCode);
		}
	}
};

export default HomePage;
