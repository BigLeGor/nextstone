import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { showErrorPage } from '../lib/utils';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Blog from '../containers/blog';
import withHeaderFooter from '../lib/withHeaderFooter';
import { getBlogState, fetchPosts, receivePosts } from '../redux/actions/blog';

import blogPageStyle from '../styles/jss/material-kit-react/pages/blog/blogPage';

const pageTitle = '研究数据';

const mapStateToProps = (state) => getBlogState(state);

const mapDispatchToProps = (dispatch) => ({ fetchPosts: (categoryId) => dispatch(fetchPosts(categoryId)) });

@withHeaderFooter(pageTitle)
@connect(mapStateToProps, mapDispatchToProps)
@withStyles(blogPageStyle)
class BlogPage extends React.Component {
	componentDidMount() {
		this.props.fetchPosts(this.props.categoryId);
	}

	render() {
		const { isFetching, posts, classes } = this.props;
		let content = null;
		if (isFetching) {
			content = (<CircularProgress className={classes.loading}/>);
		} else {
			content = (<Blog posts={posts} />);
		}
		return (
			<div className={classNames(classes.main, classes.mainRaised)}>
				{content}
			</div>
		);
	}
}

BlogPage.getInitialProps = async ({ query, res, isServer, store: { dispatch } }) => {
	const { categoryId } = query;
	// if (!categoryId && res) {
	// 	return showErrorPage(res)(404);
	// }

	// if (isServer) {
	// 	// try {
	// 		const { data: posts } = await fetchPosts(categoryId);
	// 		debugger;
	// 		dispatch(receivePosts(posts));
	// 		// console.log();
	// 	// } catch (err) {
	// 	// 	let statusCode = 500;
	// 	// 	if (err.response.status) {
	// 	// 		statusCode = err.response.status;
	// 	// 	}
	// 	// 	return showErrorPage(res)(statusCode);
	// 	// }
	// }
	return { categoryId };
};


BlogPage.propTypes = {
	classes: PropTypes.object,
	fetchPosts: PropTypes.func,
	categoryId: PropTypes.string,
	posts: PropTypes.object,
	isFetching: PropTypes.bool
};

export default BlogPage;
