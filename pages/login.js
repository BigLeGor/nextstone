import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Login from '../containers/login';
import withHeaderFooter from '../lib/withHeaderFooter';
import loginPageStyle from '../styles/jss/material-kit-react/pages/login/loginPage';

const bgImg = '/static/img/bg2.jpg';

@withHeaderFooter('LogIn Page', !!bgImg)
@withStyles(loginPageStyle)
class LogInPage extends Component {
	constructor(props) {
		super(props);
		// we use this to make the card to appear after the page has been rendered
		this.state = {
			cardAnimaton: 'cardHidden'
		};
	}

	componentDidMount() {
		// we add a hidden class to the card and after 700 ms we delete it and the transition appears
		setTimeout(
			function () {
				this.setState({ cardAnimaton: '' });
			}.bind(this),
			700
		);
	}

	render() {
		const { classes } = this.props;
		return (
			<div
				className={classes.pageHeader}
				style={{
					backgroundImage: 'url(' + bgImg + ')',
					backgroundSize: 'cover',
					backgroundPosition: 'top center'
				}}
			>
				<div className={classes.container}>
					<Login className={classes[this.state.cardAnimaton]} />
				</div>
			</div>
		);
	}
}

export default LogInPage;
