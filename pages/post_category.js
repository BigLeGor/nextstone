// import PostCategoryContainer from '../containers/postCategory';
// import withHeaderFooter from '../lib/withHeaderFooter';
// import { compose } from '../lib/utils';
// import classNames from 'classnames';
// import { withStyles } from '@material-ui/core';
// import sharedStyle from '../styles/jss/material-kit-react/sharedStyle';

// export default compose(withHeaderFooter('Post Category'), withStyles(sharedStyle))(
// 	({ classes }) => (
// 		<div className={classNames(classes.main, classes.mainRaised)}>
// 			<PostCategoryContainer />
// 		</div>
// 	))
// ;

import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import PostCategory from '../containers/postCategory';
import withHeaderFooter from '../lib/withHeaderFooter';
import { getPostCategoryState, fetchPostCategories } from '../redux/actions/postCategory';
import PostCategoryContainer from '../containers/postCategory';

import blogPageStyle from '../styles/jss/material-kit-react/pages/blog/blogPage';

const pageTitle = 'Post Category Page';

const mapStateToProps = (state) => { const tmp = getPostCategoryState(state); return tmp;};

// const mapDispatchToProps = (dispatch) => ({ fetchPostCategories: () => dispatch(fetchPostCategories()) });
const mapDispatchToProps = (dispatch) => { 
var ff = () => dispatch(fetchPostCategories());
//debugger;
return { fetchPostCategories: ff };
}

@withHeaderFooter(pageTitle)
@connect(mapStateToProps, mapDispatchToProps)
@withStyles(blogPageStyle)
class PostCategoriesPage extends React.Component {
	componentDidMount() {
		this.props.fetchPostCategories();
	}

	render() {
		// debugger;

		//this.props.fetchPostCategories();
		const { isFetching, PostCategories, classes } = this.props;
		//const pp = {'name':'aaa'};
		// debugger;
		let content = null;
		if (isFetching) {
			content = (<CircularProgress className={classes.loading}/>);
		} else {
			content = (<PostCategoryContainer postCategories={PostCategories} />);
		}
		return (
			<div>
				{content}
			</div>
		);
	}
}

PostCategoriesPage.propTypes = {
	fetchPosts: PropTypes.func,
	postCategories: PropTypes.object,
	isFetching: PropTypes.bool
};

export default PostCategoriesPage;
