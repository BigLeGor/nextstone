import GalleryWall from '../containers/galleryWall';
import withHeaderFooter from '../lib/withHeaderFooter';
import { compose } from '../lib/utils';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core';
import sharedStyle from '../styles/jss/material-kit-react/sharedStyle';

const galleryWall = ({ classes }) => (
	<div className={classNames(classes.main, classes.mainRaised)}>
		<GalleryWall />
	</div>
);

galleryWall.getInitialProps = async ({ query, res, isServer, store: { dispatch } }) => {
	const { categoryId } = query;
	return categoryId;
};

export default compose(withHeaderFooter('Gallery'), withStyles(sharedStyle))(galleryWall);
