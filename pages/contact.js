import Contact from '../containers/contact';
import withHeaderFooter from '../lib/withHeaderFooter';
import { compose } from '../lib/utils';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core';
import sharedStyle from '../styles/jss/material-kit-react/sharedStyle';

export default compose(withHeaderFooter('Contact us'), withStyles(sharedStyle))(
	({ classes }) => (
		<div className={classNames(classes.main, classes.mainRaised)}>
			<Contact />
		</div>
	))
;
