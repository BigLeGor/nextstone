import Header from '../components/Header/Header';
import HeaderLinks from '../components/Header/HeaderLinks';
import Parallax from '../components/Parallax';
import GridContainer from '../components/Grid/GridContainer';
import GridItem from '../components/Grid/GridItem';
import Footer from '../components/Footer';
import PageTitle from '../components/PageTitle';
import shortid from 'shortid';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { getMenusState } from '../redux/actions/menus';

import sharedStyle from '../styles/jss/material-kit-react/sharedStyle';

const bgImg = '/static/img/AI6.jpg';

const mapStateToProps = (state) => ({ menus: getMenusState(state) });

export default (pageTitle, customBGImg = false) => (Page) =>
	connect(mapStateToProps)(withStyles(sharedStyle)(({ classes, menus, ...props }) => {
		const linkItems = menus
			.filter(({ key, name }) => key && name)
			.map(({ key, name }) => ({ label: name, url: `/blog/${key}` }));
		const galleryMenu = menus
			.find((menu) => 'galleryMenu' in menu).galleryMenu;
		const dropdownItems = galleryMenu
			.map(({ key, name }) => ({ label: name, url: `/galleryWall/${key}` }));
		return [
			<PageTitle key={shortid.generate()}>
				{pageTitle}
			</PageTitle>,
			<Header
				key={shortid.generate()}
				brand="智库 | 人工智能行业资讯"
				rightLinks={<HeaderLinks linkItems={linkItems} dropdownItems={dropdownItems} />}
				fixed
				color="transparent"
				changeColorOnScroll={{
					height: 400,
					color: 'white'
				}}
				{...props}
			/>,
			!customBGImg && <Parallax key={shortid.generate()} image={bgImg}>
				<div className={classes.container}>
					<GridContainer>
						<GridItem>
							<div className={classes.brand}>
								<h1 className={classes.title}>{/*Nextstone CMS.*/}</h1>
								<h3 className={classes.subtitle}>
									{/*Integrate Material-UI Kit, Redux, NextJS and KeystoneJS.*/}
								</h3>
							</div>
						</GridItem>
					</GridContainer>
				</div>
			</Parallax>,
			<Page key={shortid.generate()} {...props} />,
			<Footer key={shortid.generate()} />
		];
	}));
