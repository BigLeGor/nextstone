import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import { rootReducer } from 'fast-redux';

const dev = process.env.NODE_ENV !== 'production';

const reduxThunk = applyMiddleware(thunkMiddleware);

const composeEnhancers = composeWithDevTools({
	// Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

const combineDevTools = dev ? composeEnhancers(reduxThunk) : reduxThunk;

export const initStore = (initialState = {}) => createStore(rootReducer, initialState, combineDevTools);
