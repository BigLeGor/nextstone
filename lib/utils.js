const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)));

const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds * 1000));

const showErrorPage = (res) => (statusCode = 404) => {
	res.statusCode = statusCode;
	// res.end('Not found');
	return { statusCode: res.statusCode };
};

export {
	compose,
	sleep,
	showErrorPage
};
