import ErrorPage from 'next/error';

const withError = (Component) => (props) => {
	const { statusCode, ...compProps } = props;
	if (statusCode && statusCode !== 200) {
		return <ErrorPage statusCode={statusCode} />;
	}
	return <Component {...compProps} />;
};

export default withError;
