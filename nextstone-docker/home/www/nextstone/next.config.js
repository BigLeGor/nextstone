const withSass = require('@zeit/next-sass');

/* Without CSS Modules, with PostCSS */
module.exports = withSass({
	webpack: (config, { dev, isServer }) => {
		const { use: babelLoader } = config.module.rules.find(({ use }) => use && use.loader === 'next-babel-loader');

		Object.assign(babelLoader, {
			options: {
				presets: [
					['@babel/preset-env', {
						targets: {
							browsers: 'last 2 Chrome version',
							node: true
						}
					}]
				],
				plugins: [
					'transform-decorators-legacy',
					'transform-class-properties'
				]
			}
		});

		// try to use source map in production mode, DO NOT use this unless you wanna debug on production...
		/* if (!dev) {
			config.devtool = 'source-map';

			for (const plugin of config.plugins) {
			  if (plugin.constructor.name === 'UglifyJsPlugin') {
					plugin.options.sourceMap = true;
					break;
			  }
			}
		}*/
		return config;
	}
});

/* With CSS Modules */
// module.exports = withLess({ cssModules: true })

/* With additional configuration on top of CSS Modules */
/*
module.exports = withLess({
  cssModules: true,
  webpack: function (config) {
    return config;
  }
});
*/
