docker stop nextstone && docker rm nextstone && docker rmi nextstone:0.0.1
cd ../../../
docker build -t nextstone:0.0.1 .
docker tag nextstone:0.0.1 nextstone:online
docker run --name nextstone \
	-v /home/www/nextstone/nextstone.env:/usr/local/nextstone/.env  \
	--network nextstone-net  \
	--restart on-failure:5 \
	-d nextstone:online
