docker run --name nextstone-nginx \
	-p 443:443 -p 80:80  \
	-v /home/nginx/conf.d:/etc/nginx/conf.d:ro \
	-v /home/nginx/ssl:/etc/nginx/ssl:ro  \
	-v /home/nginx/logs:/var/log/nginx \
	-v /home/nginx/nginx.conf:/etc/nginx/nginx.conf:ro \
	--restart=on-failure  \
	--network nextstone-net \
	-d nginx
