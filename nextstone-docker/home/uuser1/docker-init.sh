echo "create nextstone-net"
bash ./start-nextstone-net.sh
echo "create nextstoneDB"
rm -rf /home/mongo/data/nextstone
mkdir -p /home/mongo/data/nextstone
bash ./start-mongo.sh
echo "create nextstone-nginx"
rm -rf /home/nginx
cp ../nginx /home/nginx
bash ./start-nginx.sh
echo "create nextstone-site"
rm -rf /home/www/nextstone
mkdir -p /home/www/nextstone
cp -f ../nextstone /home/www/nextstone
bash ./start-nextstone.sh