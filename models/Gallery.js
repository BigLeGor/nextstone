var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Gallery Model
 * =============
 */
var storage = new keystone.Storage({
	adapter: keystone.Storage.Adapters.FS,
	fs: {
		path: 'uploads',
		publicPath: '/public/uploads/',
	}
});

var Gallery = new keystone.List('Gallery', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Gallery.add({
	// heroImage: { type: Types.CloudinaryImage },
	// images: { type: Types.CloudinaryImages },
	name: { type: String, required: true },
	author: { type: Types.Relationship, ref: 'User', index: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
	publishedDate: { type: Types.Date, index: true, dependsOn: { state: 'published' } },
	thumb: { type: Types.Url },
	video: { type: Types.Url },
	brief: { type: Types.Html, wysiwyg: true, height: 150 },
	thumbUpload: { type: Types.File, storage },
	categories: { type: Types.Relationship, ref: 'GalleryCategory', many: true }
});

Gallery.register();
