import Head from 'next/head';

const PageTitle = ({ children }) => {
	if (!children) {
		return null;
	}
	return (
		<Head>
			<title>{children || ''}</title>
		</Head>
	);
};

export default PageTitle;
