import Button from '../CustomButtons/Button';
import Link from 'next/link';

export default ({ children, href, className }) => (
	<Link href={href}>
		<Button color="transparent" className={className}>
			{children}
		</Button>
	</Link>
);
