import React from 'react';

// material-ui components
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

// core components
import Link from './Link';
import CustomDropdown from '../CustomDropdown';

import headerLinksStyle from '../../styles/jss/material-kit-react/components/headerLinksStyle.jsx';

const HeaderLinks = ({ classes, linkItems, dropdownItems }) => (
	<List className={classes.list}>
		{
			linkItems.map(
				(item) => (
					<ListItem className={classes.listItem}>
						<Link href={item.url} className={classes.navLink}>
							{item.label}
						</Link>
					</ListItem>
				)
			)
		}
		<ListItem className={classes.listItem}>
			<CustomDropdown
				buttonText="视频课程"
				dropdownHeader="课程类别"
				buttonProps={{
					className: classes.navLink,
					color: 'transparent'
				}}
				dropdownList={
					dropdownItems.map((item) => (
						<Link href={item.url} className={classes.navLink}>
							{item.label}
						</Link>
					))
				}
			/>
		</ListItem>
	</List>
);

export default withStyles(headerLinksStyle)(HeaderLinks);
