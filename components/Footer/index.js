import React from 'react';
// nodejs library to set properties for components
import PropTypes from 'prop-types';
// nodejs library that concatenates classes
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { List, ListItem } from '@material-ui/core';

// @material-ui/icons
import Favorite from '@material-ui/icons/Favorite';

import footerStyle from '../../styles/jss/material-kit-react/components/footerStyle.jsx';

function Footer({ ...props }) {
	const { classes, whiteFont } = props;
	const footerClasses = classNames({
		[classes.footer]: true,
		[classes.footerWhiteFont]: whiteFont
	});
	const aClasses = classNames({
		[classes.a]: true,
		[classes.footerWhiteFont]: whiteFont
	});
	return (
		<footer className={footerClasses}>
			<div className={classes.container}>
				<div className={classes.left}>
					<List className={classes.list}>
						<ListItem className={classes.inlineBlock}>
							<a href="/" className={classes.block}>
								Home
							</a>
						</ListItem>
						<ListItem className={classes.inlineBlock}>
							<a
								href="/"
								className={classes.block}
							>
								About us
							</a>
						</ListItem>
						<ListItem className={classes.inlineBlock}>
							<a href="/blog" className={classes.block}>
								Research
							</a>
						</ListItem>
						<ListItem className={classes.inlineBlock}>
							<a
								href="/login"
								className={classes.block}
							>
								Login
							</a>
						</ListItem>
					</List>
				</div>
				<div className={classes.right}>
					&copy; {1900 + new Date().getYear()} {' '}
					<a href="/" className={aClasses}>
						深圳前海智库
					</a>
				</div>
			</div>
		</footer>
	);
}

Footer.propTypes = {
	classes: PropTypes.object.isRequired,
	whiteFont: PropTypes.bool
};

export default withStyles(footerStyle)(Footer);
